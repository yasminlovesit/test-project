import React from 'react';

import HeroSection from '@/features/home/HeroSection';

const Sample = () => {
  return (
    // <Main
    //   meta={
    //     <Meta
    //       title="Next.js Boilerplate Presentation"
    //       description="Next js Boilerplate is the perfect starter code for your project. Build your React application with the Next.js framework."
    //     />
    //   }
    // >
    //   <HeroSection />
    // </Main>
    <HeroSection />
  );
};

export default Sample;
